#pragma once

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>
#include <glm/gtx/rotate_vector.hpp>

#include "mesh.hpp"
#include <vector>
#include <iostream>

MeshPtr make_tree(const std::vector<std::pair<glm::vec3, float>>& points);


void make_leafs_for_tree(const std::vector<std::pair<glm::vec3, float>>& points,
                        std::vector<glm::mat4>& leafs_model_matrices,
                        glm::vec3 root_position);
