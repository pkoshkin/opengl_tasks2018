#pragma once

#include <map>
#include <vector>
#include <string>
#include <utility>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>


struct State {
    glm::vec3 angles;
    glm::vec4 position;
    float step_distance;
    float rotate_angle;
    float invert;
    float width;
};


class LSystem {
private:
    float initial_rotate_string;
    float initial_step_distance;
    float distance_scale;
    float angle_scale;
    float start_width;
    float width_scale;
    std::map<char, std::string> rules;
    std::string initial_string;
    std::string current_string;

    glm::mat4 get_step(const State& state);
    void push_position(const State& state, std::vector<std::pair<glm::vec3, float>>& result);
    void update_state(State& state);
public:
    LSystem() = default;
    LSystem(
        const std::map<char, std::string>& rules,
        std::string initial_string,
        float rotate_angle,
        float step_distance,
        float distance_scale,
        float angle_scale,
        float start_width,
        float width_scale
    );
    void execute_iterations(uint16_t num_iterations);
    std::vector<std::pair<glm::vec3, float>> get_points();
};
