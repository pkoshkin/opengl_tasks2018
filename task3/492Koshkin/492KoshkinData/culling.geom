#version 430
 
layout(points) in;
layout(points, max_vertices = 1) out;

uniform mat3 normal_to_camera_matrix;
out vec3 vertex_position;
 
void main()
{
    if (length(normal_to_camera_matrix * gl_in[0].gl_Position.xyz) < 2000.0)
    {
        vertex_position = gl_in[0].gl_Position.xyz;
 
        EmitVertex();
        EndPrimitive();
    }
}
