#pragma once

#include "application.hpp"
#include "mesh.hpp"
#include "shader_program.hpp"
#include "texture.hpp"
#include "light_info.hpp"

#include <cmath>
#include <iostream>
#include <vector>

#include "tree.hpp"
#include "leaf.hpp"
#include "l_system.hpp"

#include <ctime>
#include <cstdlib>

#define GLM_SWIZZLE_XYZ

class TreeApplication : public Application {
private:
    MeshPtr tree;
    MeshPtr leaf;
    MeshPtr ground;

    std::vector<glm::mat4> leafs_model_matrices;
    std::vector<glm::mat4> leafs_normal_to_camera_matrices;
    std::vector<glm::mat4> trees_model_matrices;
    std::vector<glm::mat4> trees_normal_to_camera_matrices;

    LightInfo light;
    ShaderProgramPtr grass_shader;
    ShaderProgramPtr tree_shader;
    ShaderProgramPtr leaf_shader;

    std::vector<std::pair<glm::vec3, float>> points;
    LSystem system;

    TexturePtr wood_texture;
    TexturePtr grass_texture;
    TexturePtr normal_map_tree;
    TexturePtr normal_map_grass;
    TexturePtr leaf_texture;

    DataBufferPtr leafs_model_matrices_buff;
    DataBufferPtr leafs_normal_to_camera_matrices_buff;
    DataBufferPtr trees_model_matrices_buff;
    DataBufferPtr trees_normal_to_camera_matrices_buff;

    GLuint sampler;
public:
    void setup_LSystem(
        std::map<char, std::string> rules,
        std::string initialString,
        float rotate_angle,
        float step_distance,
        float distance_scale,
        float angle_scale,
        float start_width,
        float width_scale,
        int num_iterations
    ) {
        system = LSystem(
            rules, initialString,
            rotate_angle, step_distance,
            distance_scale, angle_scale,
            start_width, width_scale
        );
        system.execute_iterations(num_iterations);
        points = system.get_points();
    }

    void makeScene() override {
        std::srand(unsigned(std::time(0)));
        Application::makeScene();

        float leaf_size = 0.05;
        leaf = make_leaf(leaf_size, leaf_size / 2, leaf_size);
        ground = makeGroundPlane(100.0, 100.0);
        tree = make_tree(points);

        int sqrt_trees_num = 5;
        for (int i = 0; i < sqrt_trees_num; ++i) {
            for (int j = 0; j < sqrt_trees_num; ++j) {
                int x_shirt = std::rand() % 5;
                int y_shirt = std::rand() % 5;
                int x_direction = (std::rand() % 2) * 2 - 1;
                int y_direction = (std::rand() % 2) * 2 - 1;
                
                glm::vec3 root(i * 5 + x_shirt * x_direction, j * 5 + y_shirt * y_direction, 0.0);
                trees_model_matrices.push_back(glm::translate(glm::mat4(1.0f), root));
                make_leafs_for_tree(points, leafs_model_matrices, root);
            }
        }
        for (auto model_matrix: leafs_model_matrices) {
            leafs_normal_to_camera_matrices.push_back(glm::transpose(glm::inverse(
                model_matrix))
            );
        }
        for (auto model_matrix: trees_model_matrices) {
            trees_normal_to_camera_matrices.push_back(glm::transpose(glm::inverse(
                model_matrix))
            );
        }

        leafs_model_matrices_buff = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        leafs_model_matrices_buff->setData(
            leafs_model_matrices.size() * sizeof(float) * 4 * 4,
            leafs_model_matrices.data()
        );
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, leafs_model_matrices_buff->id());

        leafs_normal_to_camera_matrices_buff = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        leafs_normal_to_camera_matrices_buff->setData(
            leafs_normal_to_camera_matrices.size() * sizeof(float) * 4 * 4,
            leafs_normal_to_camera_matrices.data()
        );
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, leafs_normal_to_camera_matrices_buff->id());

        trees_model_matrices_buff = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        trees_model_matrices_buff->setData(
            trees_model_matrices.size() * sizeof(float) * 4 * 4,
            trees_model_matrices.data()
        );
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, trees_model_matrices_buff->id());

        trees_normal_to_camera_matrices_buff = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        trees_normal_to_camera_matrices_buff->setData(
            trees_normal_to_camera_matrices.size() * sizeof(float) * 4 * 4,
            trees_normal_to_camera_matrices.data()
        );
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, trees_normal_to_camera_matrices_buff->id());

        _cameraMover = std::make_shared<FreeCameraMover>();

        // Создаем шейдерную программу        
        grass_shader = std::make_shared<ShaderProgram>("492KoshkinData/default.vert", "492KoshkinData/grass.frag");
        tree_shader = std::make_shared<ShaderProgram>("492KoshkinData/instance.vert", "492KoshkinData/tree.frag", "492KoshkinData/culling.geom");
        leaf_shader = std::make_shared<ShaderProgram>("492KoshkinData/instance.vert", "492KoshkinData/leaf.frag", "492KoshkinData/culling.geom");
        //tree_shader = std::make_shared<ShaderProgram>("492KoshkinData/instance.vert", "492KoshkinData/tree.frag");
        //leaf_shader = std::make_shared<ShaderProgram>("492KoshkinData/instance.vert", "492KoshkinData/leaf.frag");

        // Загрузка и создание текстур
        wood_texture = loadTexture("492KoshkinData/wood_texture.jpg");
        grass_texture = loadTexture("492KoshkinData/grass_texture.jpg");
        normal_map_tree = loadTexture("492KoshkinData/wood_normal_map.jpg");
        normal_map_grass = loadTexture("492KoshkinData/grass_normal_map.png");
        leaf_texture = loadTexture("492KoshkinData/leaf_rgba.png");

        // Инициализация сэмплеров
        glGenSamplers(1, &sampler);
        glSamplerParameteri(sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glSamplerParameteri(sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glSamplerParameterf(sampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, 3);
    }

    void draw() override {
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Инициализация значений переменных освщения
        light.direction = glm::vec3(0.0, 0.0, -1.0);
        light.ambient = glm::vec3(0.2, 0.2, 0.2);
        light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        light.specular = glm::vec3(0.1, 0.1, 0.1);

        GLuint tree_texture_unit = 0;
        glBindTextureUnit(tree_texture_unit, wood_texture->texture());
        glBindSampler(tree_texture_unit, sampler);

        GLuint grass_texture_unit = 1;
        glBindTextureUnit(grass_texture_unit, grass_texture->texture());
        glBindSampler(grass_texture_unit, sampler);

        GLuint tree_normal_map_unit = 2;
        glBindTextureUnit(tree_normal_map_unit, normal_map_tree->texture());
        glBindSampler(tree_normal_map_unit, sampler);

        GLuint grass_normal_map_unit = 3;
        glBindTextureUnit(grass_normal_map_unit, normal_map_grass->texture());
        glBindSampler(grass_normal_map_unit, sampler);

        glEnable(GL_BLEND);

        // Трава
        glBlendFunc(GL_ONE, GL_ZERO);
        grass_shader->use();

        //Устанавливаем общие юниформ-переменные
        grass_shader->setMat4Uniform("view_matrix", _camera.viewMatrix);
        grass_shader->setMat4Uniform("projection_matrix", _camera.projMatrix);

        grass_shader->setVec3Uniform("light.direction", glm::mat3(_camera.viewMatrix) * light.direction);
        grass_shader->setVec3Uniform("light.La", light.ambient);
        grass_shader->setVec3Uniform("light.Ld", light.diffuse);
        grass_shader->setVec3Uniform("light.Ls", light.specular);

        grass_shader->setIntUniform("diffuse_tex", grass_texture_unit);
        grass_shader->setIntUniform("normal_map_tex", grass_normal_map_unit);

        glm::mat4 eye = glm::translate(glm::mat4(1.0f), glm::vec3(0.0, 0.0, 0.0));

        grass_shader->setMat4Uniform("model_matrix", eye);
        grass_shader->setMat3Uniform("normal_to_camera_matrix", glm::mat3(eye));
                
        ground->draw();

        // Деревья
        tree_shader->use();
        tree_shader->setVec3Uniform("light.direction", glm::mat3(_camera.viewMatrix) * light.direction);
        tree_shader->setVec3Uniform("light.La", light.ambient);
        tree_shader->setVec3Uniform("light.La", light.ambient);
        tree_shader->setVec3Uniform("light.Ld", light.diffuse);
        tree_shader->setVec3Uniform("light.Ls", light.specular);

        tree_shader->setIntUniform("diffuse_tex", tree_texture_unit);
        tree_shader->setIntUniform("normal_map_tex", tree_normal_map_unit);
        tree_shader->setMat4Uniform("view_matrix", _camera.viewMatrix);
        tree_shader->setMat4Uniform("projection_matrix", _camera.projMatrix);

        unsigned int tree_model_matrix_ssbo_index = glGetProgramResourceIndex(
            tree_shader->id(),
            GL_SHADER_STORAGE_BLOCK,
            "model_matrices_buff"
        );
        glShaderStorageBlockBinding(tree_shader->id(), tree_model_matrix_ssbo_index, 2); // 2я точка привязки
        unsigned int tree_normal_to_camera_matrix_ssbo_index = glGetProgramResourceIndex(
            tree_shader->id(),
            GL_SHADER_STORAGE_BLOCK,
            "normal_to_camera_matrices_buff"
        );
        glShaderStorageBlockBinding(tree_shader->id(), tree_normal_to_camera_matrix_ssbo_index, 3); // 3я точка привязки
        tree->drawInstanced(trees_model_matrices.size());
        /*
        for (int i = 0; i < trees_model_matrices.size(); ++i) {
            tree_shader->setMat4Uniform("model_matrix", trees_model_matrices[i]);
            tree_shader->setMat3Uniform("normal_to_camera_matrix", trees_normal_to_camera_matrices[i]);
            tree->draw();
        }
        */

        // Листья
        leaf_shader->use();

        // Устанавливаем общие юниформ-переменные
        leaf_shader->setMat4Uniform("view_matrix", _camera.viewMatrix);
        leaf_shader->setMat4Uniform("projection_matrix", _camera.projMatrix);

        // Инициализация значений переменных освщения
        light.specular = glm::vec3(0.2, 0.2, 0.2);
        leaf_shader->setVec3Uniform("light.direction", glm::mat3(_camera.viewMatrix) * light.direction);
        leaf_shader->setVec3Uniform("light.La", light.ambient);
        leaf_shader->setVec3Uniform("light.La", light.ambient);
        leaf_shader->setVec3Uniform("light.Ld", light.diffuse);
        leaf_shader->setVec3Uniform("light.Ls", light.specular);

        GLuint leaf_unit = 2;
        glBindTextureUnit(leaf_unit, leaf_texture->texture());
        glBindSampler(leaf_unit, sampler);
        leaf_shader->setIntUniform("diffuse_tex", leaf_unit);

        glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO);

        unsigned int leaf_model_matrix_ssbo_index = glGetProgramResourceIndex(
            leaf_shader->id(),
            GL_SHADER_STORAGE_BLOCK,
            "model_matrices_buff"
        );
        glShaderStorageBlockBinding(leaf_shader->id(), leaf_model_matrix_ssbo_index, 0); // 0я точка привязки
        unsigned int leaf_normal_to_camera_matrix_ssbo_index = glGetProgramResourceIndex(
            leaf_shader->id(),
            GL_SHADER_STORAGE_BLOCK,
            "normal_to_camera_matrices_buff"
        );
        glShaderStorageBlockBinding(leaf_shader->id(), leaf_normal_to_camera_matrix_ssbo_index, 1); // 1я точка привязки
        leaf->drawInstanced(leafs_model_matrices.size());

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }
};
