#include "tree.hpp"

void normalize(glm::vec3& vector) {
    vector = glm::vec3(
        vector[0] / glm::length(vector),
        vector[1] / glm::length(vector),
        vector[2] / glm::length(vector)
    );
}


glm::vec3 modify_vector(glm::vec3 vector, 
                        glm::vec3 first_point,
                        glm::vec3 second_point) {
    float distance = glm::distance(first_point, second_point);
    glm::vec3 projection(second_point[0], second_point[1], first_point[2]);
    glm::mat4 model_matrix = glm::translate(glm::mat4(1.0f), first_point);
    if (projection != first_point) {
        float sin = glm::distance(projection, first_point) / distance;
        float angle = std::asin(sin);
        glm::vec3 axis = glm::cross(glm::vec3(0, 0, 1), projection - first_point);
        model_matrix = glm::rotate(model_matrix, angle, axis);
    }
    glm::vec4 result = model_matrix * glm::vec4(vector, 1.0);
    return glm::vec3(result);
}


glm::vec3 modify_covector(glm::vec3 covector, 
                          glm::vec3 first_point,
                          glm::vec3 second_point) {
    float distance = glm::distance(first_point, second_point);
    glm::mat4 model_matrix = glm::translate(glm::mat4(1.0f), first_point);
    glm::vec3 projection(second_point[0], second_point[1], first_point[2]);
    if (projection != first_point) {
        float sin = glm::distance(projection, first_point) / distance;
        float angle = std::asin(sin);
        glm::vec3 axis = glm::cross(glm::vec3(0, 0, 1), projection - first_point);
        model_matrix = glm::rotate(model_matrix, angle, axis);
    }
    model_matrix = glm::transpose(glm::inverse(model_matrix));
    glm::vec4 result = model_matrix * glm::vec4(covector, 0.0);

    return glm::vec3(result);
}


void push_modified(std::vector<glm::vec3>& array,
                   float x, float y, float z,
                   glm::vec3 first_point,
                   glm::vec3 second_point,
                   bool vector=true) {
    if (vector) {
        array.push_back(modify_vector(glm::vec3(x, y, z), first_point, second_point));
    } else {
        array.push_back(modify_covector(glm::vec3(x, y, z), first_point, second_point));
    }
}


void push_segment(
    std::vector<glm::vec3>& vertices,
    std::vector<glm::vec3>& normals,
    std::vector<glm::vec2>& texcoords,
    float x_top_begin, float x_top_end,
    float y_top_begin, float y_top_end,
    float x_bottom_begin, float x_bottom_end,
    float y_bottom_begin, float y_bottom_end,
    glm::vec3 first_point, glm::vec3 second_point,
    int segment_number, int steps_num
) {
    float height = glm::distance(first_point, second_point);
    // Курочек нижней крышки
    push_modified(vertices, 0, 0, 0, first_point, second_point);
    push_modified(vertices, x_bottom_begin, y_bottom_begin, 0, first_point, second_point);
    push_modified(vertices, x_bottom_end, y_bottom_end, 0, first_point, second_point);
    for (int i = 0; i < 3; ++i) {
        push_modified(normals, 0, 0, -1, first_point, second_point, false);
    }
    // Кусочек верхней крышки
    push_modified(vertices, 0, 0, height, first_point, second_point);
    push_modified(vertices, x_top_end, y_top_end, height, first_point, second_point);
    push_modified(vertices, x_top_begin, y_top_begin, height, first_point, second_point);
    for (int i = 0; i < 3; ++i) {
        push_modified(normals, 0, 0, 1, first_point, second_point, false);
    }

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));
    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));

    // Считаем нормаль для боковой части
    glm::vec3 normal = glm::cross(
        glm::vec3(
            x_bottom_end - x_bottom_begin,
            y_bottom_end - y_bottom_begin,
            0
        ),
        glm::vec3(
            x_top_begin - x_bottom_begin,
            y_top_begin - y_bottom_begin,
            height
        )
    );
    normalize(normal);
    // Нормаль везде одинаковая. Всего 6 = 3 * 2 точек (2 треугольника)
    for (int i = 0; i < 6; ++i) {
        push_modified(normals, normal.x, normal.y, normal.z, first_point, second_point, false);
    }

    push_modified(
        vertices,
        x_top_begin,
        y_top_begin,
        height,
        first_point,
        second_point
    );
    push_modified(
        vertices,
        x_bottom_end,
        y_bottom_end,
        0,
        first_point,
        second_point
    );
    push_modified(
        vertices,
        x_top_end,
        y_top_end,
        height,
        first_point,
        second_point
    );
    texcoords.push_back(glm::vec2((float)(segment_number) / steps_num, 1.0));
    texcoords.push_back(glm::vec2((float)(segment_number + 1) / steps_num, 0.0));
    texcoords.push_back(glm::vec2((float)(segment_number + 1) / steps_num, 1.0));

    push_modified(
        vertices,
        x_top_begin,
        y_top_begin,
        height,
        first_point,
        second_point
    );
    push_modified(
        vertices,
        x_bottom_begin,
        y_bottom_begin,
        0,
        first_point,
        second_point
    );
    push_modified(
        vertices,
        x_bottom_end,
        y_bottom_end,
        0,
        first_point,
        second_point
    );

    texcoords.push_back(glm::vec2((float)(segment_number) / steps_num, 1.0));
    texcoords.push_back(glm::vec2((float)(segment_number) / steps_num, 0.0));
    texcoords.push_back(glm::vec2((float)(segment_number + 1) / steps_num, 0.0));
}


void add_cone(float bottom_width,
              float top_width,
              glm::vec3 first_point,
              glm::vec3 second_point,
              std::vector<glm::vec3>& vertices,
              std::vector<glm::vec3>& normals,
              std::vector<glm::vec2>& texcoords) {
    int steps_num = 100;
    float x_bottom = bottom_width / 2;
    float y_bottom = 0;
    float x_top = top_width / 2;
    float y_top = 0;
    const double PI = std::acos(-1);
    std::vector<float> x_begins_bottom;
    std::vector<float> y_begins_bottom;
    std::vector<float> x_ends_bottom;
    std::vector<float> y_ends_bottom;
    std::vector<float> x_begins_top;
    std::vector<float> y_begins_top;
    std::vector<float> x_ends_top;
    std::vector<float> y_ends_top;
    for (int i = 1; i <= steps_num; ++i) {
        x_begins_bottom.push_back(x_bottom);
        y_begins_bottom.push_back(y_bottom);
        x_bottom = bottom_width / 2 * std::cos(2.0 * PI * i / steps_num);
        y_bottom = bottom_width / 2 * std::sin(2.0 * PI * i / steps_num);
        x_ends_bottom.push_back(x_bottom);
        y_ends_bottom.push_back(y_bottom);

        x_begins_top.push_back(x_top);
        y_begins_top.push_back(y_top);
        x_top = top_width / 2 * std::cos(2.0 * PI * i / steps_num);
        y_top = top_width / 2 * std::sin(2.0 * PI * i / steps_num);
        x_ends_top.push_back(x_top);
        y_ends_top.push_back(y_top);
    }

    for (int i = 0; i < steps_num; ++i) {
        push_segment(
            vertices, normals, texcoords,
            x_begins_top[i], x_ends_top[i],
            y_begins_top[i], y_ends_top[i],
            x_begins_bottom[i], x_ends_bottom[i],
            y_begins_bottom[i], y_ends_bottom[i],
            first_point, second_point,
            i, steps_num
        );
    }

}


void add_leaf(const glm::vec3 first_point,
              const glm::vec3 second_point,
              const float bottom_width,
              const float top_width,
              std::vector<glm::mat4>& leafs_model_matrices) {
    //Создаем меш с цилиндром
    float distance = glm::distance(first_point, second_point);
    glm::vec3 projection(second_point[0], second_point[1], first_point[2]);

    float position_scale = (float)std::rand() / RAND_MAX;
    glm::vec3 leaf_start = first_point + position_scale * (second_point - first_point);
    glm::mat4 model_matrix = glm::translate(glm::mat4(1.0f), leaf_start);
    if (projection != first_point) {
        float sin = glm::distance(projection, first_point) / distance;
        float angle = std::asin(sin);
        glm::vec3 axis = glm::cross(glm::vec3(0, 0, 1), projection - first_point);
        model_matrix = glm::rotate(model_matrix, angle, axis);
    }

    float angle = (float)std::rand() / RAND_MAX * 2 * M_PI;
    model_matrix = glm::rotate(model_matrix, angle, glm::vec3(0.0, 0.0, 1.0));

    float skip = (bottom_width + position_scale * (top_width - bottom_width)) / 2;
    model_matrix = glm::translate(model_matrix, glm::vec3(skip, 0.0, 0.0));
    leafs_model_matrices.push_back(model_matrix);
}


void add_leafs_on_branch(glm::vec3 first_point,
                         glm::vec3 second_point,
                         float bottom_width,
                         float top_width,
                         std::vector<glm::mat4>& leafs_model_matrices,
                         const int leafs_number) {
    if (second_point[2] < first_point[2]) {
        glm::vec3 tmp_point = first_point;
        first_point = second_point;
        second_point = tmp_point;
        float tmp_width = top_width;
        top_width = bottom_width;
        bottom_width = tmp_width;
    }
    for (int i = 0; i < leafs_number; ++i) {
        add_leaf(
            first_point,
            second_point,
            bottom_width,
            top_width,
            leafs_model_matrices
        );
    }
}


void add_branch(glm::vec3 first_point,
                glm::vec3 second_point,
                float bottom_width,
                float top_width,
                std::vector<glm::vec3>& vertices,
                std::vector<glm::vec3>& normals,
                std::vector<glm::vec2>& texcoords) {
    if (second_point[2] < first_point[2]) {
        glm::vec3 tmp_point = first_point;
        first_point = second_point;
        second_point = tmp_point;
        float tmp_width = top_width;
        top_width = bottom_width;
        bottom_width = tmp_width;
    }
    //Создаем меш с цилиндром
    add_cone(bottom_width, top_width, first_point, second_point, vertices, normals, texcoords);
}


void make_leafs_for_tree(const std::vector<std::pair<glm::vec3, float>>& points,
                        std::vector<glm::mat4>& leafs_model_matrices,
                        glm::vec3 root_position) {
    for (auto it = points.begin(); it != points.end(); ++it) {
        // Сейчас тут надо перепутать y и z, не понятно почему.
        glm::vec3 start_point = it->first;
        float bottom_width = it->second;
        ++it;
        glm::vec3 end_point = it->first;
        float top_width = it->second;

        
        if (top_width < 0.05) {
            add_leafs_on_branch(
                root_position + start_point,
                root_position + end_point,
                bottom_width,
                top_width,
                leafs_model_matrices,
                3
            );
        }
    }

}


MeshPtr make_tree(const std::vector<std::pair<glm::vec3, float>>& points) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    for (auto it = points.begin(); it != points.end(); ++it) {
        // Сейчас тут надо перепутать y и z, не понятно почему.
        glm::vec3 start_point = it->first;
        float bottom_width = it->second;
        ++it;
        glm::vec3 end_point = it->first;
        float top_width = it->second;

        add_branch(
            start_point,
            end_point,
            bottom_width,
            top_width,
            vertices,
            normals,
            texcoords
        );
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}
